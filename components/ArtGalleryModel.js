import React from 'react';
import {
  AppRegistry,
  View,
  asset,
  AmbientLight,
} from 'react-360';
import Entity from 'Entity';

export default class ArtGalleryModel extends React.Component {
  render() {
     return (
       <View>
         <AmbientLight intensity={0.4}
           style={{
             color: 'white',
             transform: [{translate: [0,0,0]},]
           }}
           />
        <Entity
          style={{
            transform: [
              {translate: [1, 0, 0]},
              {scale: 1},
            ],
          }}
          source={{
            obj: asset('showroom_03.obj'),
            mtl: asset('showroom_03.mtl')
          }}
          lit={true}
        />
      </View>
     );
   }
 };
