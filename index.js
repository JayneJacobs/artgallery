import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  asset,
  AmbientLight,
  PointLight,
} from 'react-360';

import Entity from 'Entity';
import ThreeD from './components/ThreeD';
import UIScreen from './components/UIScreen';


export default class ArtGallery1 extends React.Component {
  render() {
    return (
      <View>
        <ThreeD/>
      </View>
    );
  }
};

AppRegistry.registerComponent('ArtGallery1', () => ArtGallery1);
AppRegistry.registerComponent('UIScreen', () => UIScreen);
